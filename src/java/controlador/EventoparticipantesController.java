package controlador;

import entidad.Eventoparticipantes;
import controlador.util.JsfUtil;
import controlador.util.JsfUtil.PersistAction;
import sesion.EventoparticipantesFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@ManagedBean(name = "eventoparticipantesController")
@SessionScoped
public class EventoparticipantesController implements Serializable {

    @EJB
    private sesion.EventoparticipantesFacade ejbFacade;
    private List<Eventoparticipantes> items = null;
    private Eventoparticipantes selected;

    public EventoparticipantesController() {
    }

    public Eventoparticipantes getSelected() {
        return selected;
    }

    public void setSelected(Eventoparticipantes selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private EventoparticipantesFacade getFacade() {
        return ejbFacade;
    }

    public Eventoparticipantes prepareCreate() {
        selected = new Eventoparticipantes();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/agenda2").getString("EventoparticipantesCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/agenda2").getString("EventoparticipantesUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/agenda2").getString("EventoparticipantesDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Eventoparticipantes> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/agenda2").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/agenda2").getString("PersistenceErrorOccured"));
            }
        }
    }

    public List<Eventoparticipantes> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Eventoparticipantes> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Eventoparticipantes.class)
    public static class EventoparticipantesControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            EventoparticipantesController controller = (EventoparticipantesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "eventoparticipantesController");
            return controller.getFacade().find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Eventoparticipantes) {
                Eventoparticipantes o = (Eventoparticipantes) object;
                return getStringKey(o.getIdEventoParticipantes());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Eventoparticipantes.class.getName()});
                return null;
            }
        }

    }

}

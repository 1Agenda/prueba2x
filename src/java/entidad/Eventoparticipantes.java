/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "eventoparticipantes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Eventoparticipantes.findAll", query = "SELECT e FROM Eventoparticipantes e"),
    @NamedQuery(name = "Eventoparticipantes.findByIdEventoParticipantes", query = "SELECT e FROM Eventoparticipantes e WHERE e.idEventoParticipantes = :idEventoParticipantes"),
    @NamedQuery(name = "Eventoparticipantes.findByIdEvento", query = "SELECT e FROM Eventoparticipantes e WHERE e.idEvento = :idEvento"),
    @NamedQuery(name = "Eventoparticipantes.findByConfirmado", query = "SELECT e FROM Eventoparticipantes e WHERE e.confirmado = :confirmado")})
public class Eventoparticipantes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEventoParticipantes")
    private Integer idEventoParticipantes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idEvento")
    private int idEvento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "confirmado")
    private boolean confirmado;
    @JoinColumn(name = "idAutoRegistro", referencedColumnName = "idAutoRegistro")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private AutoRegistro idAutoRegistro;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "eventoParticipantesidEventoParticipantes", fetch = FetchType.EAGER)
    private Collection<Agenda> agendaCollection;

    public Eventoparticipantes() {
    }

    public Eventoparticipantes(Integer idEventoParticipantes) {
        this.idEventoParticipantes = idEventoParticipantes;
    }

    public Eventoparticipantes(Integer idEventoParticipantes, int idEvento, boolean confirmado) {
        this.idEventoParticipantes = idEventoParticipantes;
        this.idEvento = idEvento;
        this.confirmado = confirmado;
    }

    public Integer getIdEventoParticipantes() {
        return idEventoParticipantes;
    }

    public void setIdEventoParticipantes(Integer idEventoParticipantes) {
        this.idEventoParticipantes = idEventoParticipantes;
    }

    public int getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(int idEvento) {
        this.idEvento = idEvento;
    }

    public boolean getConfirmado() {
        return confirmado;
    }

    public void setConfirmado(boolean confirmado) {
        this.confirmado = confirmado;
    }

    public AutoRegistro getIdAutoRegistro() {
        return idAutoRegistro;
    }

    public void setIdAutoRegistro(AutoRegistro idAutoRegistro) {
        this.idAutoRegistro = idAutoRegistro;
    }

    @XmlTransient
    public Collection<Agenda> getAgendaCollection() {
        return agendaCollection;
    }

    public void setAgendaCollection(Collection<Agenda> agendaCollection) {
        this.agendaCollection = agendaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEventoParticipantes != null ? idEventoParticipantes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Eventoparticipantes)) {
            return false;
        }
        Eventoparticipantes other = (Eventoparticipantes) object;
        if ((this.idEventoParticipantes == null && other.idEventoParticipantes != null) || (this.idEventoParticipantes != null && !this.idEventoParticipantes.equals(other.idEventoParticipantes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Eventoparticipantes[ idEventoParticipantes=" + idEventoParticipantes + " ]";
    }
    
}

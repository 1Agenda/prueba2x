/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "reunionempleados")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reunionempleados.findAll", query = "SELECT r FROM Reunionempleados r"),
    @NamedQuery(name = "Reunionempleados.findByIdReunionEmpleados", query = "SELECT r FROM Reunionempleados r WHERE r.idReunionEmpleados = :idReunionEmpleados"),
    @NamedQuery(name = "Reunionempleados.findByIdreunion", query = "SELECT r FROM Reunionempleados r WHERE r.idreunion = :idreunion")})
public class Reunionempleados implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idReunionEmpleados")
    private Integer idReunionEmpleados;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idreunion")
    private int idreunion;
    @JoinColumn(name = "idEmpleados", referencedColumnName = "idEmpleados")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Empleados idEmpleados;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reunionEmpleadosidReunionEmpleados", fetch = FetchType.EAGER)
    private Collection<Agenda> agendaCollection;

    public Reunionempleados() {
    }

    public Reunionempleados(Integer idReunionEmpleados) {
        this.idReunionEmpleados = idReunionEmpleados;
    }

    public Reunionempleados(Integer idReunionEmpleados, int idreunion) {
        this.idReunionEmpleados = idReunionEmpleados;
        this.idreunion = idreunion;
    }

    public Integer getIdReunionEmpleados() {
        return idReunionEmpleados;
    }

    public void setIdReunionEmpleados(Integer idReunionEmpleados) {
        this.idReunionEmpleados = idReunionEmpleados;
    }

    public int getIdreunion() {
        return idreunion;
    }

    public void setIdreunion(int idreunion) {
        this.idreunion = idreunion;
    }

    public Empleados getIdEmpleados() {
        return idEmpleados;
    }

    public void setIdEmpleados(Empleados idEmpleados) {
        this.idEmpleados = idEmpleados;
    }

    @XmlTransient
    public Collection<Agenda> getAgendaCollection() {
        return agendaCollection;
    }

    public void setAgendaCollection(Collection<Agenda> agendaCollection) {
        this.agendaCollection = agendaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReunionEmpleados != null ? idReunionEmpleados.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reunionempleados)) {
            return false;
        }
        Reunionempleados other = (Reunionempleados) object;
        if ((this.idReunionEmpleados == null && other.idReunionEmpleados != null) || (this.idReunionEmpleados != null && !this.idReunionEmpleados.equals(other.idReunionEmpleados))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Reunionempleados[ idReunionEmpleados=" + idReunionEmpleados + " ]";
    }
    
}

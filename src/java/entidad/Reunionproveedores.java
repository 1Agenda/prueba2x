/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "reunionproveedores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reunionproveedores.findAll", query = "SELECT r FROM Reunionproveedores r"),
    @NamedQuery(name = "Reunionproveedores.findByIdReunionProveedores", query = "SELECT r FROM Reunionproveedores r WHERE r.idReunionProveedores = :idReunionProveedores"),
    @NamedQuery(name = "Reunionproveedores.findByIdReunion", query = "SELECT r FROM Reunionproveedores r WHERE r.idReunion = :idReunion")})
public class Reunionproveedores implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idReunionProveedores")
    private Integer idReunionProveedores;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idReunion")
    private int idReunion;
    @JoinColumn(name = "idProveedores", referencedColumnName = "idProveedores")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Proveedores idProveedores;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reunionProveedoresidReunionProveedores", fetch = FetchType.EAGER)
    private Collection<Agenda> agendaCollection;

    public Reunionproveedores() {
    }

    public Reunionproveedores(Integer idReunionProveedores) {
        this.idReunionProveedores = idReunionProveedores;
    }

    public Reunionproveedores(Integer idReunionProveedores, int idReunion) {
        this.idReunionProveedores = idReunionProveedores;
        this.idReunion = idReunion;
    }

    public Integer getIdReunionProveedores() {
        return idReunionProveedores;
    }

    public void setIdReunionProveedores(Integer idReunionProveedores) {
        this.idReunionProveedores = idReunionProveedores;
    }

    public int getIdReunion() {
        return idReunion;
    }

    public void setIdReunion(int idReunion) {
        this.idReunion = idReunion;
    }

    public Proveedores getIdProveedores() {
        return idProveedores;
    }

    public void setIdProveedores(Proveedores idProveedores) {
        this.idProveedores = idProveedores;
    }

    @XmlTransient
    public Collection<Agenda> getAgendaCollection() {
        return agendaCollection;
    }

    public void setAgendaCollection(Collection<Agenda> agendaCollection) {
        this.agendaCollection = agendaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReunionProveedores != null ? idReunionProveedores.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reunionproveedores)) {
            return false;
        }
        Reunionproveedores other = (Reunionproveedores) object;
        if ((this.idReunionProveedores == null && other.idReunionProveedores != null) || (this.idReunionProveedores != null && !this.idReunionProveedores.equals(other.idReunionProveedores))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Reunionproveedores[ idReunionProveedores=" + idReunionProveedores + " ]";
    }
    
}

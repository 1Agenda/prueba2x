package validators;

import entidad.Lugares;
import java.util.Vector;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("capcityValidator")
public class CapcityValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {

        String people = value.toString();

        HtmlSelectOneMenu uiInputConfirmCapcity = (HtmlSelectOneMenu) component.getAttributes().get("capcity");
        Object capcity = uiInputConfirmCapcity.getSubmittedValue();

        int capacidad = 0;
        entidad.Lugares lugar = new entidad.Lugares();
        UISelectItems uiInputConfirmCapcity2 = (UISelectItems) component.getAttributes().get("capcity2");
        Vector capcity2 = (Vector) uiInputConfirmCapcity2.getValue();
        for (int i = 0; i < capcity2.size(); i++) {
            lugar = (entidad.Lugares) capcity2.get(i);
            if (Integer.parseInt(capcity + "") == lugar.getIdLugares()) {
                capacidad = lugar.getCapcidad();
                i = capcity2.size();
            }
        }

        // Let required="true" do its job.
        if (capcity == null) {
            return;
        }

        if (Integer.parseInt(people) > capacidad) {
            throw new ValidatorException(new FacesMessage("la capacidad de " + lugar.getNombre()
                    + " esta excedida su capacidad maxima es de " + lugar.getCapcidad()));
        }

    }

}

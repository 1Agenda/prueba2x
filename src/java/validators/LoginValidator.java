
package validators;

/**
 *
 * @author ARMAND
 */


import controlador.UsuariosController;
import entidad.Lugares;
import entidad.Usuarios;
import java.util.Vector;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;


//@ManagedBean(name = "loginv")
@FacesValidator("LoginValidator")
public class LoginValidator implements Validator {
    
    
    @ManagedProperty(value = "#{usuariosController}")
    private UsuariosController usuario;

    String user;
    String pwd;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) throws ValidatorException {

        user = value.toString();
        System.out.println("USER: " +user);

        HtmlInputSecret uiInputPwd= (HtmlInputSecret) component.getAttributes().get("pwd");
        pwd=uiInputPwd.getSubmittedValue().toString();
        System.out.println("PWD: "+pwd);
        Usuarios u = new Usuarios();
        
        u.setUser(user);
        u.setPassword(pwd);
        usuario = new UsuariosController();
        System.out.println("MANAGED PRIME "+ usuario.toString());
        
        usuario.login(u);
        //usuario.login(u);
        
    }
    

}

